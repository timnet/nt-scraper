package com.netisov.timofei.scraper.util;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

  /**
   * Redirects to index page
   * where routing is handles by frontend
   * @return
   */
  @RequestMapping({"categories", "companies/*", "update-data"})
  public String index() {
    return "forward:/";
  }
}