package com.netisov.timofei.scraper.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class GoogleResults {
  private ResponseData responseData;


  @Getter
  @Setter
  @ToString
  public static class ResponseData {
    private List<Result> results;
  }

  @Getter
  @Setter
  @ToString
  public static class Result {
    private String url;
    private String title;
  }
}
