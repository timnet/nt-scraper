package com.netisov.timofei.scraper.api.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Getter
@Setter
@ApiModel(value = "JobStartStatus", description = "Status of started job")
@ToString
public class JobStatus {
  @ApiModelProperty(value = "Job Status", position = 10)
  private String status;
  @ApiModelProperty(value = "Current step", position = 20)
  private String step;
  @ApiModelProperty(value = "Percent competed", position = 30)
  private int percentCompleted;

}
