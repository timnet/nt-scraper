package com.netisov.timofei.scraper.api.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Getter
@ApiModel(value = "Company", description = "Company representation")
@ToString
public class CompanyRepresentation {
  @ApiModelProperty(value = "name", position = 10)
  private String name;
  @ApiModelProperty(value = "search results", position = 20)
  List<CompanyResultRepresentation> companyResults;


  @AllArgsConstructor(access = AccessLevel.PROTECTED)
  @NoArgsConstructor(access = AccessLevel.PROTECTED)
  @Builder
  @Getter
  @ApiModel(value = "CompanyResultRepresentation", description = "Search result of company")
  public static class CompanyResultRepresentation {
    @ApiModelProperty(value = "title", position = 10)
    private String title;
    @ApiModelProperty(value = "url", position = 20)
    private String url;
  }
}
