package com.netisov.timofei.scraper.api.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Getter
@ApiModel(value = "CategoryCompaniesRepresentation", description = "Category with companies")
@ToString
public class CategoryCompaniesRepresentation {
  @ApiModelProperty(value = "category name", position = 10)
  private String categoryName;
  @ApiModelProperty(value = "list", position = 20)
  private List<CompanyRepresentation> companies;

}
