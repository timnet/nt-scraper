package com.netisov.timofei.scraper.api.rest;

import com.netisov.timofei.scraper.ImportJob;
import com.netisov.timofei.scraper.api.rest.model.CategoryCompaniesRepresentation;
import com.netisov.timofei.scraper.api.rest.model.CompanyRepresentation;
import com.netisov.timofei.scraper.api.rest.model.JobStatus;
import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.repository.CategoryRepository;
import com.netisov.timofei.scraper.repository.CompanyRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ScraperResource {

  private final CategoryRepository categoryRepository;
  private final CompanyRepository companyRepository;
  private final ImportJob importJob;
  private volatile boolean jobIsRunning;

  public ScraperResource(CategoryRepository categoryRepository, CompanyRepository companyRepository, ImportJob importJob) {
    this.categoryRepository = categoryRepository;
    this.companyRepository = companyRepository;
    this.importJob = importJob;
  }

  /**
   * Start singleton job in separate thread
   * to scrape web pages
   */
  @GetMapping("/start-scrape")
  public void startScrape() {
    if (!jobIsRunning) {
      ExecutorService service = Executors.newSingleThreadExecutor();
      service.submit(importJob);
      jobIsRunning = true;
    }
  }

  /**
   * Returns status of current import job status;
   *
   * @return
   */
  @GetMapping("/job-status")
  public JobStatus getJobStatus() {
    return importJob.getJobStatus();
  }

  /**
   * Returns list of categories
   *
   * @return
   */
  @GetMapping("/categories")
  public List<Category> getCategories() {
    return categoryRepository.findAll();
  }

  /**
   * Returns list of companies in a category
   *
   * @param id of category
   * @return
   */
  @GetMapping("/companies/{id}")
  public CategoryCompaniesRepresentation getCompaniesByCategory(@PathVariable("id") Integer id) {

    List<CompanyRepresentation> companyRepresentations = companyRepository.findCompaniesByCategoryId(id).stream()
        .map(e -> CompanyRepresentation.builder()
            .name(e.getName())
            .companyResults(e.getSearchResults().stream()
                .map(s -> CompanyRepresentation.CompanyResultRepresentation.builder()
                    .title(s.getTitle())
                    .url(s.getUrl())
                    .build())
                .collect(Collectors.toList()))
            .build()).collect(Collectors.toList());
    return CategoryCompaniesRepresentation
        .builder()
        .categoryName(categoryRepository.findOne(id).getName())
        .companies(companyRepresentations)
        .build();
  }

}
