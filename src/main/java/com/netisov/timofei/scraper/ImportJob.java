package com.netisov.timofei.scraper;

import com.netisov.timofei.scraper.api.rest.model.JobStatus;
import com.netisov.timofei.scraper.services.ScraperService;
import lombok.Synchronized;
import org.springframework.stereotype.Component;

/**
 * Job class to execute overall
 * process of updating data,
 * has to be launched asynchronously.
 */
@Component
public class ImportJob implements Runnable {
  /**
   * Job status to share between back-end and front-end
   */
  private JobStatus jobStatus;
  /**
   * Service to launch steps
   */
  private final ScraperService scraperService;


  public ImportJob(ScraperService scraperService) {
    this.scraperService = scraperService;
  }


  @Override
  public void run() {
    setJobStatus(JobStatus.builder()
        .step("categoriesStep")
        .status("STARTED")
        .build());
    getJobStatus().setPercentCompleted(30);
    scraperService.updateCategories();
    getJobStatus().setStep("companiesStep");
    getJobStatus().setPercentCompleted(60);
    scraperService.updateCompanies();
    getJobStatus().setStep("companiesSearchResultsStep");
    getJobStatus().setPercentCompleted(80);
    scraperService.updateCompaniesSearchResults();
    getJobStatus().setStatus("COMPLETED");
    getJobStatus().setPercentCompleted(100);
  }

  @Synchronized
  public JobStatus getJobStatus() {
    return jobStatus;
  }

  @Synchronized
  private void setJobStatus(JobStatus jobStatus) {
    this.jobStatus = jobStatus;
  }
}
