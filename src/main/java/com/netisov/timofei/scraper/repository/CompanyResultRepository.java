package com.netisov.timofei.scraper.repository;

import com.netisov.timofei.scraper.domain.Company;
import com.netisov.timofei.scraper.domain.CompanyResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for search results in google.
 */
@Repository
public interface CompanyResultRepository extends JpaRepository<CompanyResult, Integer> {
  @Transactional
  @Modifying
  @Query("delete from CompanyResult c where c.company=?1")
  void deleteResultsOfCompany(Company company);
}
