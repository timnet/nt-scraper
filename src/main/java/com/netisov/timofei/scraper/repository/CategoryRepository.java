package com.netisov.timofei.scraper.repository;

import com.netisov.timofei.scraper.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for categories
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
  @Query("select c from Category c where c.name=?1")
  Category findByName(String name);
}
