package com.netisov.timofei.scraper.repository;

import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for companies.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {
  @Query("select c from Company c where c.name=?1 and c.category=?2")
  Company findCompanyByNameAndCategory(String name, Category category);

  @Query("select c from Company c where  c.category.id=?1")
  List<Company> findCompaniesByCategoryId(Integer id);
}
