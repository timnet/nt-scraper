package com.netisov.timofei.scraper.services;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.domain.Company;
import com.netisov.timofei.scraper.domain.CompanyResult;
import com.netisov.timofei.scraper.repository.CategoryRepository;
import com.netisov.timofei.scraper.repository.CompanyRepository;
import com.netisov.timofei.scraper.repository.CompanyResultRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Service class provides essential processing
 * of overall functionality
 */
@Service
public class ScraperService {
  /**
   * HOST URL to source web-site to scrap
   */
  private static final String HOST_URL = "https://www.pegaxis.com";
  /**
   * URL of source web-page with categories
   */
  private static final String CATEGORIES_URL = "https://www.pegaxis.com/services/";
  /**
   * Number of search results for company to search and store
   */
  private final long limitSearches;
  /**
   * Key of client for using Google custom Search API
   */
  private final String googleApiKey;
  /**
   * ID of clients search engine
   */
  private final String searchEngineId;

  /**
   * Timeout in ms to wait until web page is loaded
   */
  private final int TIMEOUT = 30000;
  /**
   * Repositories of entities
   */
  private final CategoryRepository categoryRepository;
  private final CompanyRepository companyRepository;
  private final CompanyResultRepository companyResultRepository;

  private final Customsearch customsearch;
  /**
   * Logger
   */
  private static final Logger log = LoggerFactory.getLogger(ScraperService.class);

  public ScraperService(@Value("${google-api.limit}") long limitSearches,
                        @Value("${google-api.key}") String googleApiKey,
                        @Value("${google-api.cse-id}") String searchEngineId,
                        CategoryRepository categoryRepository,
                        CompanyRepository companyRepository,
                        CompanyResultRepository companyResultRepository) {
    this.limitSearches = limitSearches;
    this.googleApiKey = googleApiKey;
    this.searchEngineId = searchEngineId;
    this.categoryRepository = categoryRepository;
    this.companyRepository = companyRepository;
    this.companyResultRepository = companyResultRepository;
    Customsearch.Builder builder = new Customsearch.Builder(new NetHttpTransport(), new JacksonFactory(), httpRequest -> {
      // set connect and read timeouts
      httpRequest.setConnectTimeout(TIMEOUT);
      httpRequest.setReadTimeout(TIMEOUT);
    });
    customsearch = builder.build();
  }

  /**
   * Loads the web page with categories list and
   * Updates categories in local DB
   */
  public void updateCategories() {
    Document categoriesPage;
    try {
      categoriesPage = Jsoup.connect(CATEGORIES_URL)
          .timeout(TIMEOUT)
          .get();
      categoriesPage.select("a.service_list_title")
          .forEach(el -> {
            String name = el.text();
            Category category = categoryRepository.findByName(name);
            String url = el.attr("href");
            if (category == null) {
              categoryRepository.save(Category.builder()
                  .name(name)
                  .url(url)
                  .build());
            } else if (!category.getUrl().equals(url)) {
              category.setUrl(url);
            }
          });
    } catch (IOException e) {
      log.error("Unable to parse page of categories");
    }
  }

  /**
   * Loads list of companies from correspondent
   * pages of categories and srtores categories
   * in local DB
   */
  public void updateCompanies() {
    categoryRepository.findAll()
        .forEach(c -> {
          try {
            Document companiesPage = Jsoup.connect(HOST_URL.concat(c.getUrl()))
                .timeout(TIMEOUT)
                .get();
            companiesPage.select("div.company-name a")
                .forEach(el -> {
                  String name = el.text();
                  Company company = companyRepository.findCompanyByNameAndCategory(name, c);
                  if (company == null) {
                    companyRepository.save(Company.builder()
                        .name(name)
                        .category(c)
                        .build());
                  }
                });
          } catch (IOException e) {
            log.error(String.format("Unable to parse page companies page in category %s", c.getName()), e);
          }
        });
  }

  /**
   * Gets list of companies and launches
   * searches in google in several threads
   * and waits 15 minutes until all threads finish their work
   */
  public void updateCompaniesSearchResults() {
    try {
      ExecutorService executorService = Executors.newFixedThreadPool(5);
      companyRepository.findAll()
          .forEach(c -> executorService.submit(() -> updateSearchResultsForCompany(c)));
      executorService.awaitTermination(15, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      log.error("Unable to update search results for companies");
    }

  }

  /**
   * Searches and stores search results for
   * given company
   *
   * @param company
   */
  private synchronized void updateSearchResultsForCompany(Company company) {
    try {
      List<Result> results = search(company.getName());
      companyResultRepository.deleteResultsOfCompany(company);
      results
          .forEach(r -> companyResultRepository.save(CompanyResult.builder()
              .company(company)
              .url(r.getLink())
              .title(r.getTitle())
              .build()));
    } catch (IOException e) {
      log.error(String.format("Unable to get google search results for a company %s", company.getName()));
    }
  }

  /**
   * Search keyword using Google Custom Search API
   *
   * @param keyword
   * @return
   * @throws IOException
   */
  private List<Result> search(String keyword) throws IOException {

    Customsearch.Cse.List list = customsearch.cse().list(keyword);
    list.setKey(googleApiKey);
    list.setCx(searchEngineId);
    list.setNum(limitSearches);
    Search results = list.execute();
    return results.getItems();
  }

}
