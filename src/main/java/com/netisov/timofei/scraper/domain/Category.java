package com.netisov.timofei.scraper.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * DB entity for category item.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "category")
public class Category {

  @Id
  @GeneratedValue(generator = "sqlite")
  @TableGenerator(name = "sqlite", table = "sqlite_sequence",
      pkColumnName = "name", valueColumnName = "seq",
      pkColumnValue = "category")
  @Column(name = "id")
  private Integer id;

  @NotNull
  @Size(max = 128)
  @Column(name = "name")
  private String name;

  @NotNull
  @Size(max = 2000)
  @Column(name = "url")
  private String url;

  @Builder
  public Category(String name, String url) {
    Objects.requireNonNull(name);
    Objects.requireNonNull(url);
    this.name = name;
    this.url = url;
  }

}
