package com.netisov.timofei.scraper.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "company")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Setter
/**
 * DB entity for company item.
 */
public class Company {
  @Id
  @GeneratedValue(generator = "sqlite")
  @TableGenerator(name = "sqlite", table = "sqlite_sequence",
      pkColumnName = "name", valueColumnName = "seq",
      pkColumnValue = "company")
  @Column(name = "id")
  private Integer id;

  @NotNull
  @Size(max = 256)
  @Column(name = "name")
  private String name;


  @NotNull
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category_id")
  private Category category;

  @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
  private List<CompanyResult> searchResults;

  @Builder
  public Company(String name, Category category) {
    Objects.requireNonNull(name);
    Objects.requireNonNull(category);
    this.name = name;
    this.category = category;
  }

}
