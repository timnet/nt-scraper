package com.netisov.timofei.scraper.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
/**
 * DB entity for search results in Google by company
 */
@Entity
@Getter
@Table(name = "company_results")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CompanyResult {
  @Id
  @GeneratedValue(generator = "sqlite")
  @TableGenerator(name = "sqlite", table = "sqlite_sequence",
      pkColumnName = "name", valueColumnName = "seq",
      pkColumnValue = "company_results")
  @Column(name = "id")
  private Integer id;

  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "company_id")
  private Company company;
  @NotNull
  @Size(max = 2000)
  private String url;

  @NotNull
  @Size(max = 256)
  @Column(name = "title")
  private String title;

  @Builder
  public CompanyResult(Company company, String url, String title) {
    Objects.requireNonNull(company);
    Objects.requireNonNull(url);
    Objects.requireNonNull(title);
    this.company = company;
    this.title = title;
    this.url = url;
    this.title = title;
  }


}
