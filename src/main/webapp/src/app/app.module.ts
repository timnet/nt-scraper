import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {CategoriesComponent} from './categories/categories.component';
import {CompaniesComponent} from './companies/companies.component';
import {RouterModule, Routes} from "@angular/router";
import {NavComponent} from './nav/nav.component';
import {HttpClientModule} from "@angular/common/http";
import {CategoriesModule} from "./categories/categories.module";
import {CompaniesModule} from "./companies/companies.module";
import {UpdateDataComponent} from './update-data/update-data.component';
import {UpdateDataModule} from "./update-data/update-data.module";


const appRoutes: Routes = [
  {
    path: 'nav',
    component: NavComponent
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    data: {
      pageTitle: 'Categories'
    }
  },
  {
    path: 'companies/:id',
    component: CompaniesComponent,
    data: {
      pageTitle: 'Companies'
    }
  },
  {
    path: 'update-data',
    component: UpdateDataComponent,
    data: {
      pageTitle: 'Update data'
    }
  },
  {
    path: '',
    redirectTo: 'categories',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'categories',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    CategoriesModule,
    CompaniesModule,
    UpdateDataModule
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
