import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AgGridModule } from "ag-grid-angular";
import {CommonModule} from '@angular/common';
import {CategoryNameCell} from "./category.name.cell";
import {CategoriesComponent} from "./categories.component";
import {CategoriesService} from "./categories.service";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    AgGridModule.withComponents([CategoryNameCell]),
    CommonModule,
    RouterModule
  ],
  declarations: [
    CategoryNameCell,
    CategoriesComponent
  ],
  entryComponents: [
    CategoriesComponent,
    CategoryNameCell
  ],
  providers: [
    CategoriesService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CategoriesModule {
}
