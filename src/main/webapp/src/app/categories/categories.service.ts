import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class CategoriesService {

  private resourceUrl = 'api/categories';

  constructor(private http: HttpClient) {
  }

  query(): Observable<any> {
    return this.http.get(`${this.resourceUrl}`);
  }

}
