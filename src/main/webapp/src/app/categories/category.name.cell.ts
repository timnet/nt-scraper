import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular/main';

@Component({
  template: `<a routerLink="/companies/{{categoryId}}">{{params.value}}</a>`
})
export class CategoryNameCell implements ICellRendererAngularComp {

  public categoryId: string;
  public params: any;

  agInit(params: any): void {
    this.params = params;
    this.categoryId = this.params.data['id'];
  }

  refresh(params: any): boolean {
    return false;
  }
}
