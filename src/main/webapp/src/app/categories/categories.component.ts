import {Component, OnInit} from '@angular/core';
import {CategoriesService} from "./categories.service";
import {GridOptions} from "ag-grid";
import {CategoryNameCell} from "./category.name.cell";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html'
})
export class CategoriesComponent implements OnInit {
  gridOptions: GridOptions;
  rowData;

  constructor(private categoriesService: CategoriesService) {
    this.gridOptions = <GridOptions>{

    };
    this.gridOptions.columnDefs = this.createColumnDefs();
    this.gridOptions.enableSorting = true;
    this.gridOptions.pagination = true;
    this.gridOptions.paginationPageSize = 10;
    this.gridOptions.rowHeight = 45;
    this.gridOptions.headerHeight = 40;
  }

  // noinspection JSMethodCanBeStatic
  private createColumnDefs() {
    return [
      {headerName: "Name", field: "name", cellRendererFramework: CategoryNameCell, width: 500},
    ];
  }

  ngOnInit() {
    this.categoriesService.query().subscribe(
      (data: any) => {
        this.rowData = data;
      }
    )
  }

}
