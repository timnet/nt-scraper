import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule, NgbProgressbarModule} from "@ng-bootstrap/ng-bootstrap";
import {UpdateDataComponent} from "./update-data.component";
import {UpdateDataService} from "./update-data.service";

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    NgbProgressbarModule.forRoot()
  ],
  declarations: [
    UpdateDataComponent
  ],
  providers: [
    UpdateDataService
  ]
})
export class UpdateDataModule {
}
