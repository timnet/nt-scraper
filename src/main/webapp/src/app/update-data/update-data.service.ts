import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UpdateDataService {

  private resourceUrlStartJob = 'api/start-scrape';
  private resourceUrlProgressJob = 'api/job-status';

  constructor(private http: HttpClient) {
  }

  startJobQuery(): Observable<any> {
    return this.http.get(`${this.resourceUrlStartJob}`);
  }


  progressJobQuery(): Observable<any> {
    return this.http.get(`${this.resourceUrlProgressJob}`);
  }


}
