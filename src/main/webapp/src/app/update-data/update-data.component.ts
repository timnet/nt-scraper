import {Component} from '@angular/core';
import {UpdateDataService} from "./update-data.service";

@Component({
  selector: 'app-update-data',
  templateUrl: './update-data.component.html'
})
export class UpdateDataComponent {
  statusMessage: string;
  stepMessage: string;
  percentCompleted: number;

  stepNames = {
    categoriesStep: "Processing categories",
    companiesStep: "Processing companies",
    companiesSearchResultsStep: "Processing search for companies"
  };

  constructor(private updateDataService: UpdateDataService) {

  }


  startJob() {
    this.updateDataService.startJobQuery().subscribe(
      (data: any) => {
        this.getJobStatus();
      }
    );
  }

  getJobStatus() {
    this.updateDataService.progressJobQuery().subscribe(
      (data: any) => {
        this.statusMessage = "Update job is " + data['status'].toLowerCase();
        if (data['status'] !== 'COMPLETED') {
          setTimeout(_ => {
            this.getJobStatus();
          }, 10 * 1000);
        }
        this.percentCompleted = data['percentCompleted'];
        if (data['percentCompleted'] == 100) {
          this.stepMessage = "Done!";
        } else {
          this.stepMessage = this.stepNames[data['step']];
        }
      }
    )
  }

}

