import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CompaniesService {

  private resourceUrl = 'api/companies/';

  constructor(private http: HttpClient) {
  }

  query(id: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/${id}`);
  }

}
