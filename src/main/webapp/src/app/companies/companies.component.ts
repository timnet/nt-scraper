import {ActivatedRoute} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import {CompaniesService} from "./companies.service";
import {GridOptions} from "ag-grid";
import {CompanyNameCell} from "./company.name.cell";

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html'
})
export class CompaniesComponent implements OnInit {
  gridOptions: GridOptions;
  rowData;
  categoryName :String;

  constructor(private route: ActivatedRoute, private companiesService: CompaniesService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = this.createColumnDefs();
    this.gridOptions.enableSorting = true;
    this.gridOptions.pagination = true;
    this.gridOptions.paginationPageSize = 10;
    this.gridOptions.rowHeight = 45;
    this.gridOptions.headerHeight = 40;
  }

  // noinspection JSMethodCanBeStatic
  private createColumnDefs() {
    return [
      {headerName: "Company Title", field: "name", cellRendererFramework: CompanyNameCell, width: 500},
    ];
  }

  ngOnInit(): void {
    this.getCompany();

  }

  private getCompany() {
    this.companiesService.query(this.route.snapshot.paramMap.get('id')).subscribe(
      (data: any) => {
        this.categoryName = data['categoryName'];
        this.rowData = data['companies'];
      }
    )
  }
}
