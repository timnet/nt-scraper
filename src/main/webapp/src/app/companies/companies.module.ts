import {NgModule} from '@angular/core';
import {AgGridModule} from "ag-grid-angular";
import {CommonModule} from '@angular/common';
import {CompaniesComponent} from "./companies.component";
import {CompaniesService} from "./companies.service";
import {NgbModule, NgbPopoverModule} from "@ng-bootstrap/ng-bootstrap";
import {CompanyNameCell} from "./company.name.cell";

@NgModule({
  imports: [
    AgGridModule.withComponents([CompanyNameCell]),
    CommonModule,
    NgbModule.forRoot(),
    NgbPopoverModule.forRoot()
  ],
  declarations: [
    CompanyNameCell,
    CompaniesComponent
  ],
  entryComponents: [
    CompanyNameCell
  ],
  providers: [
    CompaniesService
  ]
})
export class CompaniesModule {
}
