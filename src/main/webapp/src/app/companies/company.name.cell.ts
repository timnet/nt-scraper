import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular/main';

@Component({
  template: `
    <ng-template #popContent>
      <ul>
        <li *ngFor="let res of results">
          <a target="_blank" href="{{res.url}}">{{res.title}}</a>
        </li>
      </ul>
    </ng-template>
    <span [ngbPopover]="popContent" container="body" placement="right"
          popoverTitle="Search results of {{title}}">{{params.value}}</span>`
})
export class CompanyNameCell implements ICellRendererAngularComp {

  public title: string;
  public results: any;
  public params: any;

  agInit(params: any): void {
    this.params = params;
    this.title = this.params.data['name'];
    this.results = this.params.data['companyResults'];
  }

  refresh(params: any): boolean {
    return false;
  }
}
