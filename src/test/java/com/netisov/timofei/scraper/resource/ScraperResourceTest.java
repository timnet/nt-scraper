package com.netisov.timofei.scraper.resource;

import com.netisov.timofei.scraper.ImportJob;
import com.netisov.timofei.scraper.api.rest.ScraperResource;
import com.netisov.timofei.scraper.api.rest.model.JobStatus;
import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.domain.Company;
import com.netisov.timofei.scraper.domain.CompanyResult;
import com.netisov.timofei.scraper.repository.CategoryRepository;
import com.netisov.timofei.scraper.repository.CompanyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ScraperResource.class)
public class ScraperResourceTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CategoryRepository categoryRepository;

  @MockBean
  private CompanyRepository companyRepository;

  @MockBean
  private ImportJob importJob;


  private Category category;

  private Company company;

  private CompanyResult companyResult;

  @Before
  public void init() {
    category = Category.builder().name("Mock category").url("https://www.pegaxis.com/services/accountants-auditors").build();
    category.setId(1);

    company = Company.builder().category(category).name("Mock company").build();


    companyResult = CompanyResult.builder()
        .title("Some site")
        .url("http://someurl.com")
        .company(company)
        .build();
    company.setSearchResults(Collections.singletonList(companyResult));

  }

  @Test
  public void testStartScrape() throws Exception {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
        "/api/start-scrape").accept(
        MediaType.APPLICATION_JSON);
    mockMvc.perform(requestBuilder);
    verify(importJob, Mockito.times(1)).run();
  }

  @Test
  public void testGetJobStatus() throws Exception {
    doReturn(JobStatus.builder().status("STARTED").step("categoriesStep").percentCompleted(30).build())
        .when(importJob).getJobStatus();
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
        "/api/job-status").accept(
        MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    String expected = "{status:'STARTED', step: 'categoriesStep', 'percentCompleted':30}";
    JSONAssert.assertEquals(expected, result.getResponse()
        .getContentAsString(), false);
  }

  @Test
  public void testGetCategories() throws Exception {
    List<Category> categoryList = Stream.of(category).collect(Collectors.toList());
    doReturn(categoryList).when(categoryRepository).findAll();
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
        "/api/categories").accept(
        MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    String expected = "[{id:1, name: 'Mock category', 'url':'https://www.pegaxis.com/services/accountants-auditors'}]";
    JSONAssert.assertEquals(expected, result.getResponse()
        .getContentAsString(), false);
  }

  @Test
  public void testGetCompaniesByCategory() throws Exception {

    doReturn(Collections.singletonList(company)).when(companyRepository).findCompaniesByCategoryId(1);

    doReturn(category).when(categoryRepository).findOne(1);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
        "/api/companies/1").accept(
        MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    String expected = "{categoryName:'Mock category', companies: [{name:'Mock company', companyResults: [{title:'Some site',url:'http://someurl.com'}]}]}";
    JSONAssert.assertEquals(expected, result.getResponse()
        .getContentAsString(), false);


  }

}

