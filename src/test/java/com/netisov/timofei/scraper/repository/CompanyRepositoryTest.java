package com.netisov.timofei.scraper.repository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.domain.Company;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DatabaseSetup("/data/repository/db-before.yml")
@DatabaseSetup("/data/repository/categories/db-before.yml")
@DatabaseSetup("/data/repository/companies/db-before.yml")
@DatabaseTearDown("/data/repository/db-teardown.yml")
public class CompanyRepositoryTest extends RepositoryTestBase {
  @Autowired
  private CompanyRepository companyRepository;
  @Autowired
  private CategoryRepository categoryRepository;

  @Test
  public void givenCompanyExists_thenOk() {
    Integer id = 1;
    List<Company> categories = companyRepository.findAll();
    Assert.assertNotNull(categories);
    Assert.assertEquals(1, categories.size());
    Assert.assertEquals(id, categories.get(0).getId());
  }

  @Test
  public void testFindCompanyByNameAndCategory() {
    Integer id = 1;
    String name = "Best ever seen";
    Category category = categoryRepository.findOne(1);

    Company company = companyRepository.findCompanyByNameAndCategory(name, category);

    Assert.assertNotNull(company);
    Assert.assertEquals(id, category.getId());
  }

  @Test
  public void testFindCompaniesByCategoryId() {
    Integer id = 1;


    List<Company> companies = companyRepository.findCompaniesByCategoryId(1);

    Assert.assertNotNull(companies);
    Assert.assertEquals(1, companies.size());
    Assert.assertEquals(id, companies.get(0).getId());
  }
}
