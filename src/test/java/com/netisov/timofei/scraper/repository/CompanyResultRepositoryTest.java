package com.netisov.timofei.scraper.repository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.netisov.timofei.scraper.domain.Category;
import com.netisov.timofei.scraper.domain.Company;
import com.netisov.timofei.scraper.domain.CompanyResult;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DatabaseSetup("/data/repository/db-before.yml")
@DatabaseSetup("/data/repository/categories/db-before.yml")
@DatabaseSetup("/data/repository/companies/db-before.yml")
@DatabaseSetup("/data/repository/companyresult/db-before.yml")
@DatabaseTearDown("/data/repository/db-teardown.yml")
public class CompanyResultRepositoryTest extends RepositoryTestBase {
  @Autowired
  private CompanyRepository companyRepository;
  @Autowired
  private CompanyResultRepository companyResultRepository;

  @Test
  public void givenCompanyResultExists_thenOk() {
    Integer companyId = 1;
    Company company = companyRepository.findOne(companyId);
    String url = "https://www.moneysmart.sg/savings-account";
    CompanyResult.CompanyResultPK pk = new CompanyResult.CompanyResultPK(company, url);
    CompanyResult companyResult = companyResultRepository.findOne(pk);
    Assert.assertNotNull(companyResult);
    Assert.assertEquals(companyId, companyResult.getCompanyResultPK().getCompany().getId());
    Assert.assertEquals(url, companyResult.getCompanyResultPK().getUrl());
    Assert.assertEquals("Best Savings Accounts 2018 - Singapore | MoneySmart.sg", companyResult.getTitle());
  }

  @Test
  public void testDeleteResultsOfCompany() {
    Integer companyId = 1;
    Company company = companyRepository.findOne(companyId);
    companyResultRepository.deleteResultsOfCompany(company);
    List<CompanyResult> list = companyResultRepository.findAll();
    Assert.assertEquals(0, list.size());
  }

}
