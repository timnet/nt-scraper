package com.netisov.timofei.scraper.repository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.netisov.timofei.scraper.domain.Category;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DatabaseSetup("/data/repository/db-before.yml")
@DatabaseSetup("/data/repository/categories/db-before.yml")
@DatabaseTearDown("/data/repository/db-teardown.yml")
public class CategoryRepositoryTest extends RepositoryTestBase {
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void givenCategoryExists_thenOk() {
        Integer id = 1;
        List<Category> categories = categoryRepository.findAll();
        Assert.assertNotNull(categories);
        Assert.assertEquals(1, categories.size());
        Assert.assertEquals(id, categories.get(0).getId());
    }

    @Test
    public void testFindByName() {
        Integer id = 1;
        String name = "Accounting";
        Category category = categoryRepository.findByName(name);

        Assert.assertNotNull(category);
        Assert.assertEquals(id, category.getId());
    }
}
