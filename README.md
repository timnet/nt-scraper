# Scraper application

This project was made on demand of Serv company by Netisov Timofei and has not any commercial value

# Functioning

This application is designated to scrap page of services at address https://www.pegaxis.com/services/ 
to save them, correspondent company names and its google search results.

Considering the fact that there are no free and legal access to google search result programmatically, even
by sending explicit http request on the page, thereby used Google Custom Search API with my personal customers 
key and my custom search engine key. Free usage of this Google Custom Search API allows 100 search per day and 
then get blocked for 24 hours. Thus, only part of company result search are covered using this key. If one wants
to fulfill overall data over all found companies, the customer key with payed limit has to be provided in 
configuration file application.yml instead of existing one - the name of correspondent value is google-api: key.
Also custom search engine key is customizable - values name is google-api: cse-id.
Application get 2 search results by default, but this value is customizable  - google-api: limit.

The application comes with partially pre-filled database, the path to physical location od database can be 
modifies by setting the property spring: datasource : url in application.yml. For instance:
 jdbc:sqlite:D:/Users/tim/Documents/serv-proj/nt-scraper/test.db
 
 Index page of application is a list of imported categories (services) with pagination.
 Each category is a link leading to list of companies in that category.
 Each category title is clickable showing and hiding a popover which contains 2 search results.
 Above list ares 2 buttons are located - categories leads to main list of categories and update data.
 
 Update data page contains one button - start update. By pressing that button launches background job 
 which scrap over pages and fills the database. Below the button a progress bar is located. Only one 
 background task can be 
 launched at same time, so that if one task is already launched pressing the button reveals current progress
 of already launched task before.
 
 Because of that sqlite database is designated for usage by one user, opening pages of application in 
 several tabs will cause an error of simultaneous db access and only first opened page will be working
 properly, please avoid using an application in several tabs.
 
The application made with division on 2 modules - backend with REST controller and frontend which 
deals with rest controller. 
REST requests can be made separately on swagger page at address http://localhost:8080/swagger-ui.html

# Launching
The application launched easily by launching a command in working directory _gradlew bootRun_ or on Windows
_gradlew.bat bootRun_. **ATTENTION!** Node.js has to be installed on your machine to be able to launch an application.